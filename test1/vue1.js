let app1 = new Vue({
    el: '#app-1',
    data: {
        message: '안녕하세요 Vue!'
    }
});


let app2 = new Vue({
    el: '#app-2',
    data: {
        message: '이 페이지는 ' + new Date() + ' 에 로드 되었습니다'
    }
});

let app3 = new Vue({
    el: '#app-3',
    data: {
        seen: true
    }
});

let app4 = new Vue({
    el: '#app-4',
    data: {
        todos: [
            { text: 'JavaScript 배우기' },
            { text: 'Vue 배우기' },
            { text: '무언가 멋진 것을 만들기' }
        ]
    }
});


let app5 = new Vue({
    el: '#app-5',
    data: {
        message: '안녕하세요! Vue.js!'
    },
    methods: {
        reverseMessage: function () {
            this.message = this.message.split('').reverse().join('')
        }
    }
});

let app6 = new Vue({
    el: '#app-6',
    data: {
        message: '안녕하세요 Vue!'
    }
});

/*
let app66 = new Vue({
    el: '#app-66 ol'
});
app66.component('todo-item', {
    template: '<li>할일 항목 하나입니다.</li>'
});
*/

Vue.component('todo1-item', {
    props: ['todo1'],
    template: '<li>{{ todo1.text }}</li>'
})

let app7 = new Vue({
    el: '#app-7',
    data: {
        groceryList: [
            { id: 0, text: 'Vegetables' },
            { id: 1, text: 'Cheese' },
            { id: 2, text: 'Whatever else humans are supposed to eat' }
        ]
    }
})

var data = { a: 1 }

// Vue인스턴스에 데이터 객체를 추가합니다.
var vm = new Vue({
    data: data
})

vm.$watch('a', function (newVal, oldVal) {
    alert("`vm.a`가 변경되면 호출 됩니다.");
})


////////////////////////
let obj8 = {
    foo: 'bar'
}

Object.freeze(obj8)

let app8 = new Vue({
    el: '#app-8',
    data: obj8
})

let app81 = new Vue({
    el: '#app-81',
    data: {dynamicId: "app-81-1"}

})

let app82 = new Vue({
    el: '#app-82',
    data: {isButtonDisabled: undefined}

})

//computed


let app83 = new Vue({
    el: '#app-83',
    data: {
        message1: '날짜',
        message2: '날짜'
    },
    methods: {
        reverseMessage1: function () {
            this.message1 = new Date() +"";
        }
    },
    computed: {
        reverseMessage2: function () {
            this.message2 = new Date() +"";
            return this.message2;
        }
    }
});


let app84 = new Vue({
    el: '#app-84',
    data: {
        firstName: 'firstName',
        lastName : 'lastName'
    },
    computed: {
        fullName: {
            // getter
            get: function () {
                return this.firstName + ' ' + this.lastName
            }/*,
            // setter
            set: function (newValue) {
                let names = newValue.split(' ')
                this.firstName = names[0]
                this.lastName = names[names.length - 1]
            }*/
        }
    }
});

let test1 = new Vue({
    el: '#test-1',
    data: {
        view : true
    }
});

let test2 = new Vue({
    el: '#test-2',
    methods: {
        say: function (message) {
            alert(message)
        },
        doThis: function (){
            alert("doThis");

        },
        warn: function (message, event) {
            // 이제 네이티브 이벤트에 액세스 할 수 있습니다
            if (event) event.preventDefault()
            alert(message)
        }
    }
})
